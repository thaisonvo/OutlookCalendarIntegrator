from flask_session import Session
from flask_sqlalchemy import SQLAlchemy
from msal import ConfidentialClientApplication

# Initialize SQLAlchemy
db = SQLAlchemy()

# Initialize Flask-Session
sess = Session()

# Initialize MSAL's ConfidentialClientApplication
def init_msal(app):
    return ConfidentialClientApplication(
        app.config['CLIENT_ID'],
        authority=app.config['AUTHORITY'],
        client_credential=app.config['CLIENT_SECRET']
    )