from flask import Blueprint, redirect, url_for, request, current_app, session

auth_bp = Blueprint('auth_bp', __name__)

@auth_bp.route('/login')
def login():
    # Retrieve MSAL's ConfidentialClientApplication from the Flask app's context
    client_app = current_app.msal_client
    
    # Generate the full URL for Microsoft's OAuth login
    auth_url = client_app.get_authorization_request_url(
        current_app.config['SCOPE'],
        redirect_uri=url_for('auth_bp.auth_response', _external=True),
        prompt='login',
    )
    return redirect(auth_url) # Redirect the user to Microsoft's login page


@auth_bp.route('/auth-response')
def auth_response():
    # Handle the response from Microsoft's OAuth server after user attemps to log in
    if 'error' in request.args:
        # Return an error message if login failed
        error_message = request.args.get('error_description', 'Unknown error occurred.')
        return f'Error during authentication: {error_message}'
    
    client_app = current_app.msal_client
    auth_code = request.args.get('code') # Extract the authorization code from the query string

    # Exchange the authorization code for a token
    token_request = client_app.acquire_token_by_authorization_code(
        auth_code,
        current_app.config['SCOPE'],
        redirect_uri=url_for('auth_bp.auth_response', _external=True),
    )

    # Handle possible errors from token exchange
    if 'error' in token_request:
        error_message = token_request.get('error_description', 'Unknown error occurred.')
        return f'Error exchanging authorization code for token: {error_message}'
    
    # Store user-specific data to session
    session['user'] = token_request.get('id_token_claims').get('oid')
    session['access_token'] = token_request.get('access_token')
    
    return redirect(url_for('main_bp.index')) # Redirect the user to the home page


@auth_bp.route('/logout')
def logout():
    # Clear the session to log out the user
    session.clear()
    return redirect(url_for('auth_bp.login')) # Redirect to the login page