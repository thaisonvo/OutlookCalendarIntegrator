from flask import Blueprint, session, redirect, url_for, render_template

main_bp = Blueprint('main_bp', __name__)

@main_bp.route('/')
def index():
    # Verify that user has been authenticated
    if not session.get('user'):
        return redirect(url_for('auth_bp.login'))
    
    return render_template('index.html', message='Successfully logged in!')
