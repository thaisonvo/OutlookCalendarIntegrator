import os

class Config:
    # OAuth configuration
    CLIENT_ID = os.getenv('CLIENT_ID')
    CLIENT_SECRET = os.getenv('CLIENT_SECRET')
    AUTHORITY = os.getenv('AUTHORITY')
    SCOPE = ['User.Read', 'Calendars.Read']

    # Flask configuration
    SECRET_KEY = os.getenv('SECRET_KEY')

    # SQLAlchemy configuration
    SQLALCHEMY_DATABASE_URI = os.getenv('SQLALCHEMY_DATABASE_URI')
    SQLALCHEMY_TRACK_MODIFICATIONS = os.getenv('SQLALCHEMY_TRACK_MODIFICATIONS')
    SESSION_TYPE = os.getenv('SESSION_TYPE')
