from flask import Flask

from .extensions import db, sess, init_msal
from .config import Config
from .views.main import main_bp
from .views.auth import auth_bp

def create_app():
    app = Flask(__name__)
    app.config.from_object(Config)

    db.init_app(app)
    
    app.config['SESSION_SQLALCHEMY'] = db
    sess.init_app(app)

    app.msal_client = init_msal(app)

    app.register_blueprint(main_bp)
    app.register_blueprint(auth_bp)

    return app
